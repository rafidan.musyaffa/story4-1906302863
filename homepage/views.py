from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'homePage.html')

def profile(request):
    return render(request, 'profilePage.html')

def hobby(request):
    return render(request, 'hobbyPage.html')

def education(request):
    return render(request, 'educationPage.html')

def sosmed(request):
    return render(request, 'socialmediaPage.html')

def contact(request):
    return render(request, 'contactmePage.html')